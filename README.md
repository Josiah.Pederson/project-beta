# CarCar

Team:

* Josiah Pederson - Service
* Erica Dippold - Sales

## Design

This is a React/Django application that provdes a REST API for the frontends of the Autosales microservice, AutoService microservice, and Inventory microservice.

# API Endpoints

## AutoSales Microservice

### `GET salespeople/`
Retrieve a list of salespeople

#### Response
```json
{
	"salespeople": [
		{
			"id": 1,
			"sales_person": "Barbara Jackson",
			"employee_num": 1
		}
	]
}
```

### `POST salespeople/`
Create a salesperson.

#### JSON Body Content
```json
{
  "sales_person": "Barbara Jackson",
  "employee_num": 1
}
```

### `GET autosales/`
Retrieve a list of Autosales

#### Response
```json
{
	"autosales": [
		{
			"id": 9,
			"price": 29000.0,
			"automobile": {
				"import_href": "/api/automobiles/1B3CC5FB2AN120189/",
				"vin": "1B3CC5FB2AN120189",
				"sold": true
			},
			"sales_rep": {
				"id": 1,
				"sales_person": "Barbara Jackson",
				"employee_num": 1
			},
			"customer": {
				"id": 1,
				"customer_name": "Winston Churchill",
				"address": "16 Diplomat Street, Florham Park, NJ 07932",
				"phone_number": "9734425678"
			}
		}
	]
}
```

### `POST autosales/`
Create an Autosale

#### JSON Body Content
```json
{
  "color": "red",
  "year": 2016,
  "vin": "1B3CC5FB2AN120888",
  "model_id": 1
}
```

### `POST customers/`
Create a Customer

#### JSON Body Content
```json
{
  "customer_name": "Winston Churchill",
  "address": "16 Diplomat Street, Florham Park, NJ 07932",
  "phone_number": "9734425678"
}
```

## Inventory Microservice

### `GET manufacturers/`
Retrieve a list of Manufacturers

#### Response
```json
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	]
}
```

### `POST manufacturers/`
Create a Manufacturer

#### JSON Body Content
```json
{
  "name": "Ford"
}
```

### `GET automobiles/`
Retrieve a list of Automobiles

#### Response
```json
{
	"autos": [
		{
			"href": "/api/automobiles/1B3CC5FB2AN120189/",
			"id": 1,
			"color": "blue",
			"year": 2016,
			"vin": "1B3CC5FB2AN120189",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Elantra",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Ford"
				}
			},
			"sold": true
		}
	]
}
```

### `POST automobiles/`
Create an automobile

#### JSON Body Content
```json
{
  "color": "red",
  "year": 2016,
  "vin": "1B3CC5FB2AN120888",
  "model_id": 1
}
```

### `PUT automobiles/:vin/`
Update value in specific automobile

#### JSON Body Content
```json
{
	"sold": true
}
```

### `GET models/`
Retrieve a list of Vehicle Models

```json
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```

### `POST models/`
Create a vehicle model

#### JSON Body Content
```json
{
  "name": "Elantra",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

## Service Microservice

### `GET/ technicians/`
Retrieve a list of Technicians

#### Response

```json
{
	"technicians": [
		{
			"technician_name": "Qui Gon Jinn",
			"employee_num": 2022,
			"id": 6
		},
    ]
}
```


### `POST/ technicians/`
Create new Technician

#### JSON Body Content

```json
{
	"technician_name": "Chuck Norris",
	"employee_num": "2012"
}
```


### `GET/ technicians/<int:pk>/`
Retrieve details of a technician specified by the database automatically assigned ID

#### Response

```json
{
	"technician_name": "Lightning McQueen",
	"employee_num": 2004,
	"id": 5
}
```


### `DELETE/ technicians/<int:pk>/`
Delete a Technician specified by the database automatically assigned ID

#### Response

````json
{"deleted": true}
````


### `GET/ service/`
Retrieve list of ServiceAppointments sorted by scheduled_time

#### Response

````json
{
	"appointments": [
		{
			"vin": "JASKJDVIASJDLGJFI",
			"owner": "Riley Davis",
			"scheduled_time": "2022-07-21T06:00:00+00:00",
			"reason": "flat tire",
			"dealer_sold": false,
			"finished": false,
			"id": 29,
			"technician": {
				"technician_name": "Jim Lahey",
				"employee_num": 2010,
				"id": 11
			}
		},
    ]
}
````


### `POST/ service/`
Create new ServiceAppointment

#### JSON Body Content

````json
{
	"vin": "1D3CF5NG2RN480298",
	"owner": "Erica Dippold",
	"scheduled_time": "2022-08-19T01:54:25+00:00",
	"technician": 13,
	"reason": "poured water into gas tank"
}
````


### `GET/ service/<int:pk>/`
Retrieve the details of a specific ServiceAppointment using the database auto assigned ID

#### Response

````json
{
	"vin": "JASKJDVIASJDLGJFI",
	"owner": "John Michelin",
	"scheduled_time": "2022-07-21T06:00:00+00:00",
	"reason": "drove car into lake",
	"dealer_sold": false,
	"finished": true,
	"id": 29,
	"technician": {
		"technician_name": "Sam Losco",
		"employee_num": 2003,
		"id": 14
	}
}
````


### `PUT/ service/<int:pk>/`
Update the finished property on the ServiceAppointment to True using the database auto assigned ID

#### JSON Body Content

````json
{"finished": true}
````


### `DELETE/ service/<int:pk>/`
Delete a specific ServiceAppointment using the database auto assigned ID

#### Response

````json
{"deleted": true}
````


### `GET/ service/inventory/`
Retrieve list of AutomobileVOs polled from Inventory microservice

#### Response

````json
{
	"inventory": [
		{
			"vin": "1C3CC5FB2AN120174"
		},
    ]
}
````


### `DELETE/ service/inventory/`
Delete the list of AutomobileVOs polled from Inventory microservice

#### Response

````json
{"deleted": true}
````


### `GET/ service/history/<str:pk>/`
Retrieve a list of finished Service Appointments for a specific vehicle using its VIN in <str:pk>

#### Response

````json
{
	"appointments": [
		{
			"vin": "1D3CF5NG2RN480298",
			"owner": "Zebra Zinc",
			"scheduled_time": "2022-06-03T04:05:00+00:00",
			"reason": "Crashed",
			"dealer_sold": true,
			"finished": true,
			"id": 32,
			"technician": {
				"technician_name": "Anthony Russo",
				"employee_num": 2019,
				"id": 16
			}
		},
    ]
}
````


## Service microservice

Service has three models:

Technician: A simple model that contains the name and employee num of a technician. In Erica and my version of CarCar, service techs have employee numbers are in the range of 2000 - 2999 and sales reps are in range of 1000 - 1999.  When using the webpage endpoint to POST a new tech, you do not need to provide an employee number because React fetches the latest tech employee number and adds one to it. To ensure that each tech has a unique employee number, the employee_num attribute has unique=True.

ServiceAppointment: This model has the VIN, owner, scheduled_time, technician (ForeignKey to Technician), and reason for appt that are all input when creating an appointment. There also are two additional properties that are important to note: 1: 'finished' is a boolean that can be changed from false to true when an appointment is completed. 2: dealer_sold compares the input vin to the AutomobileVO model and says true if the vehicle is either still in inventory or has been sold by this dealership. It is important to note that the choice to mark unsold vehicles that are in inventory as true was intentional because an unsold vehicle that needs service would be serviced at a discounted rate (employee hours and parts only).

AutomobileVO: this is a value object that polls the VIN, id_num and import_href from the Automobile model in the Inventory microservice. Though we have a sold property on Automobile, I did not deem this information necessary to be included in the service microservice poll request. The poll happens every sixty seconds. As explained in the above paragraph, these VINs are used to check whether special pricing is authorized for that vehicle.


## Sales microservice

Sales has four models:

SalesPerson: salesperson is a model that contains the name and employee num of all salespeople. The salespeople have employee numbers starting with 1000 as the minimum and 1999 as the maximum value in order to not overlap with the technicians who have employee numbers starting at 2000 and ending with 2999. The employee_num has a unique=True value so all employee numbers are distinct and unique.

Customer: customer is a model that contains the customer name, address, and phone number of all customers.

AutoSale: autosale is a model that contains the price, automobile, sales_rep, and customer. Automobile, sales_rep, and customer are all foreign key relationships linking to the other models (SalesPerson, Customer, and AutomobileVO). When an autosale is completed, a PUT request gets sent to automobile in the inventory microservice that marks that particular automobile as "sold". That sold value then gets polled over to Automobile VO, which removes the automobile from being able to be sold in the autosale dropdown list. The foreign keys of autosale have a .PROTECT feature which will keep the data intact and saved even when the data is deleted from the Inventory section.

AutomobileVO: automobileVO is a value object that polls the VIN, import_href, and sold value from the Automobile model in the Inventory microservice. The import_href and vin values have a unique=True property which prevents from duplicate values from occurring as all vehicles should have unique vin and import_href properties. The default = False for the sold model as we would like every Automobile to initially have sold as False until we make the PUT request to change the automobile in the inventory microservice which will then poll the sold=True to the AutomobileVO model.
