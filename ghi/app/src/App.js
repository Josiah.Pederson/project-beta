import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import Login from "./login";
import Hello from './hello';
import Logout from './logout';
import Signup from "./signup";
import MainPage from './MainPage';
import NewModel from './Inventory/NewModel';
import ListSales from './AutoSales/ListSales';
import ListModels from './Inventory/ListModels';
import AddAutoSale from './AutoSales/AddAutoSale';
import AddCustomer from './AutoSales/AddCustomer';
import NewAutomobile from './Inventory/NewAutomobile';
import AddTechnician from './AutoService/AddTechnician';
import AddSalesPerson from './AutoSales/AddSalesPerson';
import ServiceHistory from './AutoService/ServiceHistory';
import NewManufacturer from './Inventory/NewManufacturer';
import ListAutomobiles from './Inventory/ListAutomobiles';
import ListManufacturer from './Inventory/ListManufacturer';
import ListSalesByEmployee from './AutoSales/ListSalesByEmployee';
import AddServiceAppointment from './AutoService/AddServiceAppointment';
import ListServiceAppointments from './AutoService/ListServiceAppointments';


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/login" element={<Login/>} />
          <Route path="/signup" element={<Signup/>} />
          <Route path="/hello" element={<Hello/>} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/autosales">
            <Route path="" element={<ListSales />}/>
            <Route path="new" element={<AddAutoSale/>}/>
          </Route>
          <Route path="/salespeople">
            <Route path="" element={<ListSalesByEmployee/>}/>
            <Route path="new" element={<AddSalesPerson/>}/>
          </Route>
          <Route path="/customers">
            <Route path="new" element={<AddCustomer/>}/>
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ListManufacturer/>}/>
            <Route path="new" element={<NewManufacturer/>}/>
          </Route>
          <Route path="/models">
            <Route path="" element={<ListModels />} />
            <Route path="new" element={<NewModel />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<ListAutomobiles/>} />
            <Route path="new" element={<NewAutomobile />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<AddTechnician />} />
          </Route>
          <Route path="service">
            <Route path="" element={<ListServiceAppointments />} />
            <Route path="new" element={<AddServiceAppointment />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
