import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class AddCustomer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customerName: '',
            address: '',
            phoneNumber: '',
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
        AllowedToVisit()
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.phone_number = data.phoneNumber;
        delete data.phoneNumber
        data.customer_name = data.customerName
        delete data.customerName
        delete data.formVisible;
        delete data.successVisible;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                customerName: '',
                address: '',
                phoneNumber: '',
                formVisible: "shadow p-4 mt-4 d-none",
                successVisible: "",
            }
            this.setState(cleared);
        }
    }

    handleCustomerNameChange(event) {
        const value = event.target.value;
        this.setState({customerName: value})
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phoneNumber: value})
    }

    handleReset(event) {
        this.setState(
            {formVisible: "shadow p-4 mt-4",
            successVisible: "d-none"}
        );
    }

    render() {
        return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            Customer added successfully!
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Add another!
                        </button>
                    </div>
            <div className={this.state.formVisible}>
            <h1>New Customer Form</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleCustomerNameChange} value={this.state.customerName} max = "30" placeholder="Customer" required type="text" name="customer_name" id="customer_name" className="form-control"/>
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleAddressChange} value={this.state.address} max = "30" placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePhoneNumberChange} value={this.state.phoneNumber} min = "10" max = "10" placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default AddCustomer;
