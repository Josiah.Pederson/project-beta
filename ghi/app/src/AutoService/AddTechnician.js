import React from "react";
import AllowedToVisit from "../AllowedToVisit";

class AddTechnician extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technicianName: "",
            employeeNumber: "2000",
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };

        this.handleReset = this.handleReset.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.employee_num = data.employeeNumber;
        data.technician_name = data.technicianName;
        delete data.employeeNumber;
        delete data.technicianName;
        delete data.formVisible;
        delete data.successVisible;

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const url = "http://localhost:8080/api/technicians/";
        const request = await fetch(url, fetchConfig);
        if (request.ok) {
            const cleared = {
                employeeNumber: data.employee_num + 1,
                technicianName: "",
                formVisible: "shadow p-4 mt-4 d-none",
                successVisible: "",
            };
            this.setState(cleared);
        } else {
            console.error(request.status)
        };
    };

    handleReset(event) {
        this.setState(
            {formVisible: "shadow p-4 mt-4",
            successVisible: "d-none"}
        );
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({technicianName: value});
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState({employeeNumber: value});
    }

    async componentDidMount() {
        AllowedToVisit()
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const nextNumber = data.technicians.slice(-1)[0].employee_num + 1;
            if (nextNumber < 2000) {
                this.setState({employeeNumber: "2000"})
            } else {
                this.setState({employeeNumber: nextNumber});
            }
        } else {
            console.error(response.status)
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            Technician added successfully!
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Add another!
                        </button>
                    </div>
                    <div className={this.state.formVisible}>
                        <h1>Add new Technician</h1>
                        <form onSubmit={this.handleSubmit} id="addTechnicianForm">
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control" onChange={this.handleNameChange}
                                    required type="text" id="technicianName" placeholder="Name"
                                    name="technicianName" value={this.state.technicianName}
                                />
                                <label htmlFor="technicianName">Technician name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                    className="form-control" onChange={this.handleEmployeeNumberChange}
                                    required type="number" id="employeeNumber" max="2999" min="2000"
                                    placeholder="Employee number" disabled name="employeeNumber"
                                    value={this.state.employeeNumber}
                                />
                                <label htmlFor="employeeNumber">Employee number</label>
                            </div>
                            <div>
                                <button className="btn btn-primary">Add technician</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    };
};

export default AddTechnician;
