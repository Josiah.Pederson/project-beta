import React, {useState, useEffect, useRef} from "react";
import Carousel from 'react-bootstrap/Carousel';
import { Link } from 'react-router-dom'


const data = [
  {
   image: require('./Images/jeep.jpg'),
   caption:"Take on life in luxury",
   description:"30 Months | 0%",
   comment:"Our birthday. Your gift."
  },
  {
    image:require('./Images/mountain-car.jpg'),
    caption:"Find your vehicle.",
    description:"",
    comment:""
   },
   {
    image:require('./Images/jeep-desert.jpg'),
    caption:"All Terrains.",
    description:"Enjoy purchase, leasing, service and test drives today  ",
    comment:""
   }
]

function MainPage() {
  const [index, setIndex] = useState(0);

  // const vidRef=useRef();
  // useEffect(() => { vidRef.current.play(); },[]);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (<>
    <Carousel activeIndex={index} onSelect={handleSelect}>
       {data.map((slide, i) => {
        return (
          <Carousel.Item key={i}>
            <img
              className="d-block w-100"
              src={slide.image}
              alt="slider image"
              />
            <Carousel.Caption>
              <h3>{slide.caption}</h3>
              <p>{slide.description}</p>
              <p>{slide.comment}</p>
              <Link to={'/models'}>
                <button className="btn btn-light"> Explore</button>
              </Link>
              {/* <button className="btn btn-primary">
                <a href="/models">Login</a>
                </button> */}
            </Carousel.Caption>
          </Carousel.Item>
        )
      })}
    </Carousel>
    {/* <div className="row mt-3">
    <video
        src={speedvideo}
        ref={ vidRef }
        muted
        autoPlay
        loop
        width="1124"
      />
      </div> */}


    {/* <div className="row mt-3">
      <div className="card mb-3">
          <img className="card-img-top" src={carinterior} alt="interior"/> */}
            {/* <div className="card text-center">
              <div className="card-body">
                <h5 className="card-title">Interior of the Future</h5>
                  <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
              </div>
            </div> */}
          {/* </div>
      </div> */}
    </>
  );
}
export default MainPage;
