from django.contrib import admin
from .models import StaffUser

# Register your models here.

class StaffUserAdmin(admin.ModelAdmin):
    model = StaffUser

admin.site.register(StaffUser, StaffUserAdmin)
