from audioop import reverse
from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Salesperson(models.Model):
    sales_person = models.CharField(max_length=100)
    employee_num = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.sales_person

class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.customer_name

class Autosale(models.Model):
    price = models.FloatField()
    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="autosales",
        on_delete=models.PROTECT
    )
    sales_rep = models.ForeignKey(
        Salesperson,
        related_name="autosales",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name="autosales",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return (
            self.sales_rep.sales_person + "sold" + str(self.automobile.vin)
        )
