from django.contrib import admin
from .models import InventoryVO, Technician, ServiceAppointment

# Register your models here.

@admin.register(InventoryVO)
class InventoryVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass


@admin.register(ServiceAppointment)
class ServiceAppointmentAdmin(admin.ModelAdmin):
    pass