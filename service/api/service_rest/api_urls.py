from django.urls import path
from .views import (
    api_list_technicians,
    api_get_techncian,
    api_get_inventoryvos,
    api_list_service_appointments,
    api_get_service_appointment,
    api_vehicle_service_history,
) 

urlpatterns = [
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
    ),
    path(
        "technicians/<int:pk>/",
        api_get_techncian,
        name="api_get_techncian"
    ),
    path(
        "service/inventory/",
        api_get_inventoryvos,
        name="api_get_inventoryvos"
        ),
    path(
        "service/",
        api_list_service_appointments,
        name="api_list_service_appointments"
    ),
    path(
        "service/<int:pk>/",
        api_get_service_appointment,
        name="api_get_service_appointment",
    ),
    path(
        "service/history/<str:pk>/",
        api_vehicle_service_history,
        name="api_vehicle_service_history"
    ),
]
